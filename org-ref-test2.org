# Local IspellDict: en
#+STARTUP: showeverything

# Copyright (C) 2019 Jens Lechtenbörger
# SPDX-License-Identifier: GPL-3.0-or-later

# Neither generate table of contents nor section numbers
#+OPTIONS: toc:nil num:nil

# Enable: browser history, fragment IDs in URLs, mouse wheel, links between presentations
#+OPTIONS: reveal_history:t reveal_fragmentinurl:t
#+OPTIONS: reveal_mousewheel:t reveal_inter_presentation_links:t
#+OPTIONS: reveal_width:1400 reveal_height:1000
#+OPTIONS: timestamp:nil

#+REVEAL_TRANS: fade
#+REVEAL_THEME: white
#+REVEAL_PLUGINS: (search zoom highlight)
#+REVEAL_TITLE_SLIDE: <h1 class="title">%t</h1><h3 class="subtitle">%s</h3><h2 class="author">%a</h2><h2 class="date">%d</h2>

#+OPTIONS: reveal_klipsify_src:nil

# The following is necessary for PDF export.
# Note that even without PDF export, the used bibliography file needs
# to be declared for org-ref, which can happen in various ways.
# First, org-ref understands the printbibliography command below.
# Second, addbibresource:references.bib could be used before
# the printbibliography command if PDF export is not necessary.
# Third, org-ref-default-bibliography can be customized.

#+TITLE: Sample presentation with bibliography
#+SUBTITLE: (Press ~?~ for help; ~n~ and ~p~ for next and previous slide)
#+AUTHOR: Jens Lechtenbörger
#+DATE: August 2019

* Introduction

#+BEGIN_EXPORT latex
\begin{equation}
  \label{eqn:eqn-sin}
\sinh x
\end{equation}

#+END_EXPORT

Insert a reference cite:GA


This is a second reference cite:KF20

And a third one 


#+BEGIN_EXPORT latex
\begin{equation}
  \label{eqn:eqn-schr}
  \hbar\frac{\partial}{\partial t}\Psi(\mathbf{x},t)=-\frac{\hbar^2}{2m}\nabla^2\Psi(\mathbf{x},t)+V(\mathbf{x},t)\Psi(\mathbf{x},t)
\end{equation}

#+END_EXPORT


#+ATTR_LaTeX: scale=0.2
#+CAPTION: Plotting something new 2 label:fig:label2
[[./sin.png]]



* The image
#+ATTR_LaTeX: scale=0.2
#+CAPTION: Plotting something new 2 label:fig:label2
[[./sin.png]]


* Results


#+BEGIN_SRC python :exports code :results silent
import matplotlib.pyplot as pl

pl.plot([1,3,2], [1,3,4])
pl.savefig("sin.png")

#+END_SRC

#+ATTR_LaTeX: scale=0.2
#+CAPTION: Plotting something label:fig:label
[[./sin.png]]

The results are in Figure ref:fig:label

* An equation

The following equation holds

# [calc-mode: language: nil]
\[ \int dx x^2 = x^3 \]

We define $F_n = F_(n-1)+F_(n-2)$ for all $n > 2$.

The derivative of $\ln\left( \ln{x} \right)$ is $\frac{1}{x \ln{x}}$

is
 whose value at

 $x = 2$ is $0.721347520444$

 and at

 $x = 3$ is $0.303413075542$

 \begin{equation}
 \label{eq:1}
 \begin{pmatrix}
   2 & 3 & 4 \\
   2 & 6 & 4 \\
   3 & 8 & 5
 \end{pmatrix}
 \end{equation}


There is this thing called the Fediverse. According to Wikipedia, Fediverse (a
portmanteau of “federation” and “universe”) is the ensemble of federated servers
that are used for web publishing (i.e. social networking, microblogging or
websites) and file hosting. I came across one of such federated microblogging
platforms not long ago, writefreely, and it's flagship instance write.as . It
offers a distraction-free experience, no adds, privacy by default, and users can
even write using markdown. I guess the time has finally come to start a blog.
Being an Emacs user, though, I figured that before actually starting a blog or
doing any meaningful writing, I should write an Elisp library that allows
posting to said blogging platform. My goal is to write posts in Org Mode, have
them exported to markdown using ox-md.el, and directly pushed to the Fediverse.
Luckily, write.as offers an easy to use API. Users can write anonymous blogposts
by simply submitting POST requests with a “title” and a “body” to their RESTful
API endpoint. With an authorization token in the request header, the blogpost is
automatically associated to an user account.

And eq.  ref:eqn:eqn-sin

Now this is Fig. ref:fig:label2


* Conclusions
Org ref used for citation


* Bibliography
:PROPERTIES:
:CUSTOM_ID: bibliography
:END:

bibliography:/home/cperezm/my-presentations/references.bib

bibliographystyle:apalike
